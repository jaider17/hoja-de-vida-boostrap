var cuerpo;

function limpiarArea() {
	cuerpo = "";
};

function dibujar(dibujo) {
	document.getElementById('dinamicContent').innerHTML = dibujo;
};

document.getElementById('linkInicio').addEventListener("click", function() {
	limpiarArea();
	cuerpo = "<div class='container contenido mt-3'> <div class='row align-items-center'> <div class='col-12 col-md-6 text-center'> <h2 class='h2'>Bienvenido</h2> </div><div class='col-12 col-md-6 text-center'> <img class='img-fluid imgTraje' src='imgs/img.jpeg' alt='imagenKbron'> </div></div></div>";
	dibujar(cuerpo);
});

document.getElementById('contact').addEventListener("click", function() {
	limpiarArea();
	cuerpo = "<div class='container contenido marginAlto'> <form> <div class='form-row'> <div class='form-group col-md-6'> <label for='nombre'>Nombres</label> <input type='text' class='form-control' id='nombre' placeholder='Nombres'> </div><div class='form-group col-md-6'> <label for='apellido'>Apllido</label> <input type='text' id='apellido' class='form-control' placeholder='Apellidos'> </div></div><div class='form-row'> <div class='form-group col-md-6'> <label for='inputEmail4'>Email</label> <input type='email' class='form-control' id='inputEmail4' placeholder='Email'> </div><div class='form-group col-md-6'> <label for='inputNumTel'>Número de teléfono</label> <input type='number' class='form-control' placeholder='300-000-0000'> </div></div><div class='row'> <div class='col-12 col-md-6'> <fieldset class='form-group mt-3'> <div class='row'> <legend class='col-form-label col-sm-2 pt-0'>Sexo</legend> <div class='col-sm-10'> <div class='form-check'> <input class='form-check-input' type='radio' name='radioM' id='maculino' value='maculino' checked> <label class='form-check-label' for='maculino'> Masculino</label> </div><div class='form-check'> <input class='form-check-input' type='radio' name='radioF' id='femenino' value='femenino'> <label class='form-check-label' for='femenino'> Femenino</label> </div><div class='form-check'> <input class='form-check-input' type='radio' name='radioO' id='otros' value='otros' > <label class='form-check-label' for='otros'> Otros </label> </div></div></div></fieldset> </div><div class='col-12 col-md-6'> <div class='form-group row'> <div class='col-sm-2'>Ocupación</div><div class='col-sm-10'> <div class='form-check'> <input class='form-check-input' type='checkbox' id='chkEst'> <label class='form-check-label' for='chkEst'> Estudiante</label> </div><div class='form-check'> <input class='form-check-input' type='checkbox' id='chkEmp'> <label class='form-check-label' for='chkEmp'> Empleado. </label> </div></div></div></div></div><div class='form-group'> <label for='inputAddress'>Dirección</label> <input type='text' class='form-control' id='inputAddress' placeholder='Cll x Cra y - 16'> </div><div class='form-group'> <label for='inputAdiDirec'>Especificacion</label> <input type='text' class='form-control' id='inputAdiDirec' placeholder='Piso, apartamento, edifico, referncia, etc...'> </div><div class='form-row'> <div class='form-group col-md-4'> <label for='inputCity'>Ciudad</label> <input type='text' class='form-control' id='inputCity' placeholder='Medellin'> </div><div class='form-group col-md-4'> <label for='inputZip'>Departamento</label> <input type='text' class='form-control' id='inputZip' placeholder='Antioquia'> </div><div class='form-group col-md-4'> <label for='inputState'>Pais</label> <select id='inputState' class='form-control'> <option selected>Colombia</option> <option>Colombia</option> <option>Colombia prro</option> </select> </div></div><div class='form-group'> <label for='exampleFormControlTextarea1'>Mensaje</label> <textarea class='form-control' id='txtAmensaje' placeholder='Escribe aqui tu mensaje. <3' rows='5' ></textarea> </div><button type='submit' class='transicion btn btn-outline-drak btnDark'>Enviar</button> <button type='reset' class='transicion btn btn-outline-drak btnDark'>Cancelar</button> </form> </div>";
	dibujar(cuerpo);
});

document.getElementById('misDP').addEventListener("click", function() {
	limpiarArea();
	cuerpo = '<div class="container contenido text-center mt-3"> <div class="row"> <div class="col-12"> <h2> Datos personales </h2> </div></div><div class="row"> <div class="col-12 col-md-6 col-lg-3 offset-lg-3"> <h5 class="h5"> Nombres </h5> </div><div class="col-12 col-md-6 col-lg-3"> <p><b><i>Jaider Orozco Pineda</i></b></p></div></div><div class="row"> <div class="col-12 col-md-6 col-lg-3 offset-lg-3"> <h5 class="h5"> Dirección </h5> </div><div class="col-12 col-md-6 col-lg-3"> <p><b>Carrera </b><i> 90B </i><b>A #</b> <i>15 </i> <i>-24</i></p></div></div><div class="row"> <div class="col-12 col-md-6 col-lg-3 offset-lg-3"> <h5 class="h5"> Fecha de nacimiento </h5> </div><div class="col-12 col-md-6 col-lg-3"> <p><i>17 </i>de <b>Marzo </b>de <i>2000</i></p></div></div><div class="row"> <div class="col-12 col-md-6 col-lg-3 offset-lg-3"> <h5 class="h5"> Teléfono de contacto </h5> </div><div class="col-12 col-md-6 col-lg-3"> <p><i>314552354</i></p></div></div></div>';
	dibujar(cuerpo);
});

document.getElementById('estudios').addEventListener("click", function() {
	limpiarArea();
	cuerpo = '<div class="container contenido text-center mt-3"> <div class="row"> <div class="col-12"> <h2> Estudios </h2> </div><div class="col-12 col-md-6 col-lg-3 offset-lg-3"> <div class="row"> <div class="col-12"> <h5 class="h5"> Bachillerato </h5> </div></div><div class="row"> <div class="col-12"> <h6 class="h6"> I.E Escuela Normal Superior</h6> </div></div><div class="row"> <div class="col-12"> <b><i>Culminado</i></b> </div></div><div class="row"> <div class="col-12"> <p><i>2017</i></p></div></div></div><div class="col-12 col-md-6 col-lg-3"> <div class="row"> <div class="col-12"> <h5 class="h5"> Técnico laboral en desarrollo de software</h5> </div></div><div class="row"> <div class="col-12"> <h6 class="h6">Fundacion Educativa Cesde</h6> </div></div><div class="row"> <div class="col-12"> <p>En curso</p></div></div><div class="row"> <div class="col-12"> <p><i>2018 - 2</i></p></div></div></div></div></div>';
	dibujar(cuerpo);
})

document.getElementById('exper').addEventListener("click", function() {
	limpiarArea();
	cuerpo = '<div class="container contenido"> <div class="row mt-3"> <div class="col-12 text-center"> <h2 class="h2">Experiencia</h2> </div></div><div class="accordion" id="accordionExample"> <div class="card carta"> <div class="card-header cartaCabecera" id="headingOne"> <h5 class="mb-0"> <button class="btn btn-link btnLink py-3 my-1" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Experiencia 1. </button> </h5> </div><div id="collapseOne" class="collapse mx-0" aria-labelledby="headingOne" data-parent="#accordionExample"> <div class="card-body acordionCuerpo"> <div class="row"> <div class="col-12 col-md-6"> <h4 class="h5">Empresa</h4> </div><div class="col-12 col-md-6"> <p>Ninguna</p></div></div><div class="row"> <div class="col-12 col-md-6"> <h4 class="h5">Cargo</h4> </div><div class="col-12 col-md-6"> <p>No aplica</p></div></div><div class="row"> <div class="col-12 col-md-6"> <h4 class="h5">Labores Realizadas</h4> </div><div class="col-12 col-md-6"> <ul> <li>No aplica</li></ul> </div></div><div class="row"> <div class="col-12 col-md-6"> <h4 class="h5">Periodo laborado.</h4> </div><div class="col-12 col-md-6"> <p>No aplica</p></div></div></div></div></div><div class="card carta"> <div class="card-header cartaCabecera" id="headingTwo"> <h5 class="mb-0"> <button class="btn btn-link btnLink py-3 my-1" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">';
	dibujar(cuerpo);
});	

document.ready()